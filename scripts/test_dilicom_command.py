#!/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarez@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import json
import os

import yaml

from search.models.models import Card
from search.models.models import CardType
from search.models.models import Basket
# from search.models.models import Preferences

from search.dilicom_command import *


"""

Usage:

./manage.py runscript test_dilicom_command
"""

def do_run(*args):
    """
    """

    user = get_ftp_username()
    password = get_ftp_password()
    gln = get_user_gln()
    host = get_ftp_host()
    if not user or not password:
        print("WARN: no Dilicom FTP username or password found.")
        exit(1)

    import ipdb; ipdb.set_trace()
    txt = generate_internet_format(Basket.auto_command_copies())
    FTP_send(txt, host, user, password)


def run(*args):
    try:
        do_run(args)
    except Exception as e:
        print(e)
