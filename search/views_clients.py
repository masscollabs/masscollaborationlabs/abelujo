# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarez@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import unicode_literals

import pendulum
import toolz
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render

# from abelujo import settings
from search import models
from search.models import Preferences
from search.models.utils import get_logger
from search.models.utils import price_fmt

log = get_logger()

@login_required
def clients_list(request, *args, **kw):
    template = 'search/clients_list.html'
    if request.method == 'GET':
        count = models.Client.objects.count()
        clients = models.Client.get_clients(
            # XXX: will need archiving clients.
            order_by="-created",
            to_dict=True,
        )
        return render(
            request,
            template,
            {
                'clients': clients,
                'nb_clients': count,
            },
        )

@login_required
def client_show(request, pk):
    template = 'search/client_show.html'
    if request.method == 'GET':
        client = models.Client.objects.filter(id=pk).first()

        if not client:
            return HttpResponseRedirect(reverse('clients_list'))

        client_sells = models.SoldCards.objects.filter(sell__client=client)

        # trick, 2022-09-08:
        # Our Contact/Client did not have a timestamp model, so
        # all clients got a dummy created date.
        # If we find an older sell for them, change their created date.
        very_first_sell = client_sells.order_by("created").first()
        if very_first_sell and client and very_first_sell.created < client.created:
            client.created = very_first_sell.created
            client.save()

        client_sells = client_sells.order_by("-created")
        total_client_sells = client_sells.count()
        last_sells = client_sells.all()[0:50]
        today = pendulum.datetime.today()
        year_sells = client_sells.filter(created__year=today.year)
        last_year_sells = client_sells.filter(created__year=today.year - 1)
        month_sells = year_sells.filter(created__month=today.month)
        previous_month = today.subtract(months=1)
        last_month_sells = year_sells.filter(created__month=previous_month.month)

        ongoing_reservations = client.reservations()

        default_currency = Preferences.get_default_currency()

        total_month = 0
        for it in month_sells:
            if it and it.price_sold is not None:
                total_month += it.price_sold
        total_month = price_fmt(total_month, default_currency)

        total_last_month = 0
        for it in last_month_sells:
            if it and it.price_sold is not None:
                total_last_month += it.price_sold
        total_last_month = price_fmt(total_last_month, default_currency)

        total_year = 0
        for it in year_sells:
            if it and it.price_sold is not None:
                total_year += it.price_sold
        total_year = price_fmt(total_year, default_currency)

        total_last_year = 0
        for it in last_year_sells:
            if it and it.price_sold is not None:
                total_last_year += it.price_sold
        total_last_year = price_fmt(total_last_year, default_currency)

        # See what shelves come most of the client's sells.
        shelves = []
        shelves_frequencies = {}
        shelves = []
        for it in client_sells:
            if it and it.card and hasattr(it.card, 'shelf'):
                shelves += [it.card.shelf]

        shelves_frequencies = toolz.frequencies(shelves)
        for sh, nb in shelves_frequencies.iteritems():
            percentage = (nb / float(total_client_sells)) * 100
            shelves_frequencies[sh] = {}
            shelves_frequencies[sh]['frequency'] = nb
            shelves_frequencies[sh]['percentage'] = percentage
            shelves_frequencies[sh]['percentage_repr'] = "{:.2f}".format(percentage)
        # Back to a list, sort by percentage.
        shelves_frequencies = shelves_frequencies.items()
        shelves_frequencies = sorted(
            shelves_frequencies,
            key=lambda it: it[1].get('percentage'),
            reverse=True,
        )
        shelves_frequencies = shelves_frequencies[:5]

        return render(
            request,
            template,
            {
                'client': client,
                'last_sells': last_sells,
                'year_sells': year_sells,
                'month_sells': month_sells,
                'total_month': total_month,
                'total_last_month': total_last_month,
                'total_year': total_year,
                'total_last_year': total_last_year,
                'shelves_frequencies': shelves_frequencies,
                'ongoing_reservations': ongoing_reservations,
            },
        )
