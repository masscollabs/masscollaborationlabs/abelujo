# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarez@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.
from __future__ import unicode_literals

import json
import os

import pendulum
from django.http import JsonResponse
from django.template.loader import get_template
from django.utils import translation
from django.utils.translation import ugettext as _
from weasyprint import HTML

from abelujo import settings
from search import sms_sender
from search.models import bill_utils
from search.models import Basket
from search.models import Bill
from search.models import Card
from search.models import CardType
from search.models import Preferences
from search.models import Sell
from search.models import users

from search.models.users import Client
from search.models.users import Reservation
from search.models.utils import enrich_cards_dict_for_quantity_in_command
from search.models.utils import get_logger
from search.models.utils import is_truthy
from search.models.utils import is_invalid
from search.models.utils import price_fmt
from search.models.common import ALERT_ERROR
# from search.models.common import ALERT_INFO
from search.models.common import ALERT_SUCCESS
# from search.models.common import ALERT_WARNING
from .utils import _is_truthy
from .utils import _is_falsy


log = get_logger()

def clients(request, **response_kwargs):
    """
    Get clients.

    Requires the abelujo-api-token header (if settings.FEATURE_USE_API_TOKEN is enabled).

    Params:
    - query: string. Filter clients by name (ignore accents and case).
    - is_institution: bool (1, t, "true" are truthy). Return all institutions. This ignores the query.
    """
    api_token = request.GET.get('abelujo-api-token')
    header_token = request.META.get('ABELUJO-API-TOKEN') or request.META.get('HTTP_ABELUJO_API_TOKEN')

    data = {"data": [],
            "alerts": [],
            "message": None,
            "message_status": None,
            "status": 200, }

    # XXX: copied from api. Use as decorator.
    if settings.FEATURE_USE_API_TOKEN and settings.API_TOKEN not in [api_token, header_token]:
        log.debug("Requesting datasource, but api tokens do NOT match")
        data['message_status'] = 401
        data['status'] = 401
        data['message'] = "authentication error"
        data['alerts'] = "erreur d'authentication"
        return JsonResponse(data)

    if request.method == 'GET':
        try:
            res = []
            params = request.GET
            query = params.get('query')
            is_institution = params.get('is_institution')
            ongoing_reservations = None

            if is_institution is not None:
                is_institution = is_truthy(is_institution)
                res = Client.objects.filter(is_institution=is_institution)
                res = [it.to_dict() for it in res]
                # querying is_institution AND a query is to be done.
                return JsonResponse({'data': res})

            if query:
                res = Client.search(query, to_dict=True, limit=20)
                # Does this client have ongoing reservations?
                check_reservations = params.get('check_reservations')
                check_reservations = _is_truthy(check_reservations)
                ongoing_reservations = None
                client_id = None
                if check_reservations and res:
                    for idx, client_dict in enumerate(res):
                        if client_dict.get('id'):
                            client_id = client_dict['id']
                            ongoing_reservations = Reservation.client_has_reservations(client_id)
                            res[idx]['ongoing_reservations'] = ongoing_reservations

                            # But how many of them are in stock, ready to be sold?
                            ready_reservations = Reservation.client_has_ready_reservations(client_id)
                            res[idx]['ready_reservations'] = ready_reservations
                        else:
                            res[idx]['ongoing_reservations'] = 0
                            res[idx]['ready_reservations'] = 0

            else:
                res = Client.get_clients(to_dict=True)
            return JsonResponse({'data': res})
        except Exception as e:
            log.error(u"error getting clients: {}".format(e))
            return JsonResponse({'data': None})

def bill_from_basket():
    pass

def is_only_books(cards):
    type_book = CardType.get_book_type()
    for it in cards:
        if it and it.card_type != type_book:
            return False
    return True

def bill(request, *args, **response_kwargs):
    """
    Create a bill or an estimate, as a PDF file.

    From either:
    - the given products (list of ids)
      - each has an optional discount.
      - for the given client.
    - an existing bill id.
    - or a given basket id.
    - bill_or_estimate: 1 is bill, 2 is estimate, 3 is bill of the sell..
    - bon_de_commande: string

    3: bill of a sell: no need to generate a new unique Bill id. The document generated at the sell is not really a bill, as in "accounting document". It's only for the client.
    On the contrary, a bill generated for an institutional client in a List is an accounting document.

    For a shipping receipt ("bon de livraison"), use the views.py method, direct URL. See baskets.html.

    """
    if request.method == 'GET':
        log.debug("/api/bill: don't use GET, use POST requests")
        return

    template = 'pdftemplates/pdf-bill-main.html'

    # ids, prices, quantities
    params = {}
    try:
        params = json.loads(request.body)
    except Exception as e:
        log.error('Sell bill: could not decode json body: {}\n{}. Referer: {}'.format(
            e, request.body, request.META.get('HTTP_REFERER')))

    to_ret = bill_utils.create_bill(params, write_pdf=True)

    # try:
    #     with open(filepath, 'wb') as f:
    #         HTML(string=sourceHtml).write_pdf(target=f.name)

    #     # with open(details_filepath, 'wb') as ff:
    #     #     HTML(string=details_html).write_pdf(target=ff.name)

    #     response = JsonResponse(to_ret)
    # except Exception as e:
    #     log.error("Error writing bill in pdf to {}: {}".format(filepath, e))
    #     response = JsonResponse({'status': 400})

    # We embed the file content into the response and we open it on the client.
    response = JsonResponse(to_ret)
    return response

def card_reservations(request, pk, **kw):
    """
    Return the list of clients that reserved this card.
    """
    to_ret = {
        'data': {},
        'status': ALERT_SUCCESS,
        'alerts': [],
    }
    if request.method == 'POST':
        # params = kw.copy()
        # client_id = params.get('client_id')
        reservations = Reservation.get_card_reservations(pk, to_dict=True)
        to_ret['data'] = reservations
        return JsonResponse(to_ret)

def reserved_cards(request, **kw):
    """
    Return the list of cards that this client reserved.

    Used in the sell to import the cards a client reserved (and the
    ones that are available in the stock right now, quantity > 0).

    The returned card objects must be augmented for the sell:
    price_orig, quantity to sell, quantity in the command basket…

    Return: JsonResponse with object status, data, alerts.
    """
    to_ret = {
        'data': {},
        'status': ALERT_SUCCESS,
        'alerts': [],
    }
    params = request.GET.copy()
    if request.method == 'GET':
        client_id = None
        if params:
            client_id = params.get('client_id')
        # XXX: is_ready depends on the quantity in stock at the moment of the reservation.
        # We could have false negatives here? See Reservation.get_reservations()
        qs = Reservation.objects.exclude(card__isnull=True) \
                            .exclude(client__isnull=True) \
                            .exclude(card_id__isnull=True) \
                            .exclude(archived=True) \
                            .exclude(is_ready=False)
        if client_id:
            qs = qs.filter(client=client_id)

        # Only the ones with quantity > 0 in stock?
        in_stock = params.get('in_stock')
        in_stock = _is_truthy(in_stock)
        if in_stock:
            qs = qs.filter(card__quantity__gte=0)

        cards = []
        for resa in qs:
            card_dict = resa.card.to_dict()
            card_dict['quantity_sell'] = resa.nb or 1
            cards.append(card_dict)

        # res = [it.to_dict() for it in cards]  # PERF: slow with hundreds.
        res = cards

        # Enrich result with quantity in the command list.
        auto_command = Basket.auto_command_basket()
        ids = [it['id'] for it in res]
        basket_copies = auto_command.basketcopies_set.filter(card__id__in=ids).select_related()
        res = enrich_cards_dict_for_quantity_in_command(res, basket_copies)
        # Enrich for the sell view (in addition of quantity_sell above)
        for card in res:
            card['price_sold'] = card['price']
            card['price_orig'] = card['price']

        to_ret['data'] = res
        return JsonResponse(to_ret)


def all_reservations(request, *args, **kw):
    """
    Return all reservations.
    If client_id is given, filter by this client (return her ongoing reservations).
    """
    # currently for CLI usage.
    params = request.GET.copy()
    client_id = None
    if params:
        client_id = params.get('client_id')
    qs = Reservation.objects.exclude(card__isnull=True) \
                            .exclude(client__isnull=True) \
                            .exclude(card_id__isnull=True) \
                            .exclude(archived=True) \
                            .exclude(is_ready=False)
    if client_id:
        qs.filter(client=client_id)
    res = [it.to_dict() for it in qs.all()]
    return JsonResponse(res, safe=False)


def reservation_notify_sms(request, *args, **kw):
    """
    Send a text message (SMS) to a client.

    POST only.

    Args:
    - client_pk: int
    - body: SMS body (string)

    Save the SMS per client.

    Only if FEATURE_SMS is truthy.
    """
    # see abelujo-js.js
    # body is: client_pk: 99; <body text directly here>
    to_ret = {
        'data': {},
        'status': ALERT_SUCCESS,
        'alerts': [],
    }
    if request.method == 'POST':
        client_pk = None
        body = request.body

        MAX_SMS_LENGTH = 3000
        if len(body) > MAX_SMS_LENGTH:
            to_ret['status'] = ALERT_ERROR
            to_ret['alerts'].append("The SMS is too long.")
            return JsonResponse(to_ret)

        try:
            client_pk = int(kw.get('pk'))
        except Exception as e:
            log.error('Error parsing SMS API body: {}'.format(e))
            to_ret['status'] = ALERT_ERROR
            to_ret['alerts'].append("Oops! Internal error. We could not send the message.")
            return JsonResponse(to_ret)

        try:
            client_obj = Client.objects.filter(pk=client_pk).first()
        except Exception as e:
            log.error(e)
            to_ret['status'] = ALERT_ERROR
            to_ret['alerts'].append("Oops! Internal error. We could not send the message.")
            return JsonResponse(to_ret)

        if not client_obj:
            to_ret['status'] = ALERT_ERROR
            to_ret['alerts'].append("We could not find this client!")
            return JsonResponse(to_ret)

        if not client_obj.mobilephone:
            to_ret['status'] = ALERT_ERROR
            to_ret['alerts'].append("This client has no mobile phone!")
            return JsonResponse(to_ret)

        # Correctly formatted mobile phone number:
        receiver = client_obj.mobilephone_for_sms()

        # Send it:
        status, messages_obj = sms_sender.send_sms(body=body,
                                                   receiver=receiver,
                                                   client=client_obj)

        if not status or _is_falsy(status):
            to_ret['status'] = False
            to_ret['alerts'].append("The SMS was not sent.")

        return JsonResponse(to_ret)
