#!/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarez@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.
from __future__ import unicode_literals

import httplib
import json
import logging
import unittest

from django.core.urlresolvers import reverse
from django.test import TestCase

import factory
from factory.django import DjangoModelFactory
from search import models
from search.models.api import getSellDict
from search.models.api import list_from_coma_separated_ints
from search.models.api import list_to_pairs
from search.models.common import (ALERT_SUCCESS,  # noqa: F401
                                  ALERT_ERROR,
                                  ALERT_INFO,
                                  ALERT_WARNING)
from search.models.utils import get_logger
from search.models import bill_utils

from .factories import CardFactory, ClientFactory, SellsFactory, PlaceFactory, PreferencesFactory

log = get_logger()


class BillsTest(TestCase):

    def setUp(self):
        # Create one needed "book" card type.
        self.type_book = "book"
        typ = models.CardType(name=self.type_book, vat=5.5)
        typ.save()
        self.card = CardFactory.create()
        self.sell = SellsFactory.create()
        # Preferences, default Place.
        self.place = PlaceFactory.create()
        self.client = ClientFactory.create()
        self.basket = models.InstitutionBasket(
            name="basket test",
            client=self.client,
        )
        self.basket.save()
        self.basket.add_copy(self.card, nb=3)

        self.preferences = PreferencesFactory()
        self.preferences.default_place = self.place
        self.preferences.save()

        # GET params to generate an institution bill.
        self.params = {
            'basket_id': self.basket.pk,
        }

    def tearDown(self):
        pass

    def test_true(self):
        to_ret = bill_utils.create_bill(self.params, with_data_dict=True)
        self.assertEqual(200, to_ret['status'])
        self.assertTrue(to_ret['filename'].startswith('Bill'))
        self.assertTrue(to_ret['outhtml'])  # pdf content.
        # rounding?
        # self.assertEqual(3 * 9.90, to_ret['bill_data_dict']['total_to_pay'])
        self.assertTrue(to_ret['bill_data_dict']['total_to_pay'] >= 29.7)
