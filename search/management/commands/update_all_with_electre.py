#!/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarez@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

"""
Update all the cards' information after an Electre lookup, including:

- the distributor
- the publisher
- thickness, weight etc
- etc

This can take several minutes with thousands of cards.

Options:

- verbose: -V t

"""
from __future__ import unicode_literals

import time

from django.core.management.base import BaseCommand

from pyelectre import search_on_electre
from search.models import Card

# py2/3
try:
    input = raw_input
except NameError:
    pass


class Command(BaseCommand):

    def add_arguments(self, parser):
        # parser.add_argument(
        #     '-V',
        #     dest='verbose',
        #     help='debug output.',
        # )
        pass

    def update_all(self):
        # All cards with an ISBN.
        cards = Card.objects.filter(isbn__isnull=False, quantity__gt=0)
        self.stdout.write("Updating {} cards in stock with an ISBN.".format(cards.count()))

        confirmation = True
        confirmation = raw_input("Continue ? [Y/n]")
        if confirmation == "n":
            exit(0)

        self.stdout.write("Searching all on Electre...")

        cards_updated = []
        cards_not_updated = []
        count_ok = 0
        for i, card in enumerate(cards):
            if not card.isbn and not (pyelectre.is_isbn(card.isbn)):
                continue
            bklist, errors = search_on_electre(card.isbn)
            bk = list(filter(lambda it: it['isbn'] == card.isbn, bklist))
            if bk:
                bk = bk[0]

            if not bk:
                self.stdout.write("No result for {}. Passing.".format(card.isbn))
                cards_not_updated.append(card)
                continue

            pub_name = ""
            if bk.get('publishers'):
                pub_name = bk.get('publishers')[0]
            if not pub_name:
                print("Could not find the name of distributor for {}. Exiting.".format(bk.get('publishers')[0]))
                exit(1)

            print("* {} - updating {}".format(i, card.isbn))

            try:
                gln = bk.get('distributor_gln')
                if gln:
                    gln = str(gln)   # XXX: to fix in pyelectre too.
                if not gln:
                    print("Could not get distributor GLN from {}. Exiting.".format(bk))
                    exit(1)
                Card.update_from_dict(card, bk,
                                      distributor_gln=gln,
                                      publisher_name=pub_name)

                cards_updated.append(card)
                count_ok += 1
            except Exception as e:
                self.stdout.write(" ! failed for {}: {}".format(bk.get('isbn'), e))

            # Updating hundreds of cards is resource heavy.
            time.sleep(0.1)

        self.stdout.write("-------------------")
        self.stdout.write("Cards OK: {}.".format(count_ok))
        self.stdout.write("Cards updated:{}".format(len(cards_updated)))
        self.stdout.write("Cards not updated:{}".format(len(cards_not_updated)))
        # self.stdout.write("\n".join([it.isbn for it in cards_not_updated]))
        self.stdout.write("Done.")

    def handle(self, *args, **options):
        self.stdout.write("Go...")
        try:
            self.update_all()
        except KeyboardInterrupt:
            self.stdout.write("User abort.")
            self.stdout.write("Cards updated: {}".format(len(cards_updated)))
