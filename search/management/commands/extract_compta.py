#!/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarez@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

"""
Get numbers about sold prodcuts.

For each book or product sold, write a line in the CSV.

Generate a report of the sells between 2 dates.

Create a CSV file with these columns: "vente id", "date", "ISBN", "montant HT", "TVA", "montant TTC", "règlement".

Usage:

rlwrap ./manage.py extract_compta [--from yyyy-mm-dd --to yyyy-mm-dd]

Note: the TO date is excluded. Give the next day.

Without arguments, the scripts will ask for two dates.

Ensure to have the same PAYMENT_CHOICES in config.py that the ones used in prod.

-------

Notations pour bilans comptables, avec option --french-labels:


NOM DU COMPTE | COMPTE COMPTABLE
--------------|----------------

VENTE HT à 0% 70700000

VENTE HT à 5,5% 70700010

VENTE HT à 20% 70700040

CB 58000010

CHEQUE 58000030

ESPECES 58000020

TVA  5,5% 44571400

TVA 20% 44570000

"""
from __future__ import unicode_literals

import datetime

import unicodecsv
from django.core.management.base import BaseCommand

from search.models import SoldCards
from search.models.common import get_payment_abbr
from search.models.utils import is_book
from search.views_utils import Echo

# py2/3
try:
    input = raw_input
except NameError:
    pass


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--ids',
                            dest="ids",
                            action="store",
                            help="card ids, followed by their selling prices, followed by their quantities, all separated by comas.")

        parser.add_argument('--from',
                            dest="from_date",
                            action="store",
                            help="Start date.")

        parser.add_argument('--to',
                            dest="to_date",
                            action="store",
                            help="Date to (excluding. Give the next day).")

        parser.add_argument('--french-labels',
                            dest="french_labels",
                            action="store_true",
                            help="Utiliser les numéros de compte comptable pour les moyens de paiement, ajout d'une colonne facture/avoir et d'une colonne pour type de vente avec compte comptable.")

    def run(self, *args, **options):
        """
        """
        DATE_FORMAT = "%Y-%m-%d"
        csv_filename = "ventes-compta.csv"
        french_labels = options.get("french_labels")

        self.stdout.write("Comptabilité périodes de ventes.")
        input_from = None
        if options.get("from_date"):
            input_from = options.get("from_date")
        else:
            input_from = input("Date from ? [YYYY-mm-dd] ")

        input_to = None
        if options.get("to_date"):
            input_to = options.get("to_date")
        else:
            input_to = input("Date to (excluding. Enter the next day) ? [YYYY-mm-dd] ")

        date_from = datetime.datetime.strptime(input_from, DATE_FORMAT)
        date_to = datetime.datetime.strptime(input_to, DATE_FORMAT)
        print(date_to, date_from)

        soldcards = SoldCards.objects.filter(created__gt=date_from).filter(created__lt=date_to)

        # write CSV
        pseudo_buffer = Echo()
        writer = unicodecsv.writer(pseudo_buffer, delimiter=b';')
        content = writer.writerow(b"")

        rows = []
        header = ("vente id", "date", "ISBN", "montant HT", "TVA", "montant TTC", "Quantité", "règlement")

        if french_labels:
            header += ("facture/avoir", "compte comptable")

        if header:
            rows.insert(0, header)

        for soldcard in soldcards:
            # if not soldcard.card:
            #     # Skip deleted cards.
            #     continue
            row = [
                soldcard.sell.pk,
                soldcard.created,
                soldcard.card.isbn if soldcard.card else "???",
            ]
            # montant HT
            tva = 5.5 if is_book(soldcard.card) else 20  # XXX: hard-coded french VAT.
            if soldcard and soldcard.card and soldcard.price_sold == soldcard.card.price:
                montant_ht = soldcard.card.price_excl_vat
            else:
                montant_ht = soldcard.price_sold - soldcard.price_sold * tva / 100
            row.append(montant_ht)

            row.append(tva)
            row.append(soldcard.price_sold)  # TTC

            # quantity (facture / avoir)
            row.append(soldcard.quantity)

            # mode de règlement
            payment = soldcard.sell.payment
            payment_abbr = ""
            if payment:
                payment_abbr = get_payment_abbr(int(payment))
            if payment_abbr == 'card':
                payment_abbr = 'CB'

            if french_labels:
                if payment_abbr == 'CB':
                    payment_abbr = "58000010"
                elif payment_abbr == 'CHEQUE':
                    payment_abbr = "58000030"
                elif payment_abbr.startswith('ESPECE'):
                    payment_abbr = "58000020"
                else:
                    payment_abbr = "58000040"

            row.append(payment_abbr)
            # optionnel: remise.

            # (optionnel) avoir/remise, compte comptable.
            if french_labels:
                if soldcard.quantity < 0:
                    row.append("avoir")
                else:
                    row.append("facture")

                if tva == 5.5:
                    label = "44571400"
                elif tva == 20:
                    label = "44570000"
                else:
                    label = ""

                # voir aussi: vente HT à 0, 5.5 ou 20%

                row.append(label)

            rows.append(row)

        content = b"".join([writer.writerow(it) for it in rows])
        with open(csv_filename, "w") as f:
            f.write(content)

        self.stdout.write("Written to file: {}".format(csv_filename))

    def handle(self, *args, **options):
        try:
            self.run(*args, **options)
        except KeyboardInterrupt:
            self.stdout.write("User abort.")
