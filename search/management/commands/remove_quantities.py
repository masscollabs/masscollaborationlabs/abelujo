#!/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarez@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

"""
Custom management command.

Remove (substract) these cards' quantities from the stock.

Typical use: you are given a CSV file with ISBN;quantities, for some reasons you are asked to remove them.
=> maybe it lacks options to close a list?
"""
from __future__ import unicode_literals

from django.core.management.base import BaseCommand

from search.models import Card, Preferences
from search.views_utils import extract_all_isbns_quantities

# py2/3
try:
    input = raw_input
except NameError:
    pass


class Command(BaseCommand):

    help = "Set all the cards' quantities to zero, in every places. This doesn't touch lists, commands or inventories."

    def add_arguments(self, parser):
        parser.add_argument(
            '-i',
            dest='input',
            help='CSV file of input.',
        )
        parser.add_argument(
            '-q',
            dest='default_quantity',
            help='Default quantity. Ignore the quantity given in the input file.',
        )

    def handle(self, *args, **options):
        self.stdout.write("-------------------")
        self.stdout.write("You are going to SUBSTRACT those book quantities:")
        confirmation = raw_input("Continue ? [Y/n]")
        if confirmation == "n":
            exit(0)

        csvfile = options.get('input')
        if not csvfile.endswith('csv'):
            self.stdout.write("Please give a .csv file as argument (for now).")
            exit(1)

        lines = []
        with open(csvfile, "r") as f:
            lines = f.readlines()

        isbns_quantities, msgs = extract_all_isbns_quantities(lines)
        if msgs:
            self.stdout.write(msgs)

        place = Preferences.get_default_place()

        for isbn_qty in isbns_quantities:
            card = Card.objects.filter(isbn=isbn_qty[0]).first()
            if not card:
                self.stdout.write("ISBN not found: {}".format(isbn_qty[0]))
            else:
                qty = isbn_qty[1]
                default_quantity = options.get('default_quantity')
                if default_quantity is not None:
                    qty = int(default_quantity)

                self.stdout.write("Removing x{} of {}".format(qty, isbn_qty[0]))
                place.remove(card, quantity=qty)

        self.stdout.write("All done.")
