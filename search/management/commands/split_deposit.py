#!/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarez@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

"""
./manage.py split-deposit --id 1

But why?
You imported user data and created one unique deposit with all books.
Now you want to create as many deposits as there are suppliers (distributors), as the import script should have done.
"""

from django.core.management.base import BaseCommand

from search.models import Deposit


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument(
            '--id',
            dest='deposit_id',
            help='',
        )

    def handle(self, *args, **options):
        # depos = Deposit.objects.all()
        deposit_id = options.get('deposit_id')
        depo = Deposit.objects.get(pk=deposit_id)
        # Get the deposit state, the copys and quantities.
        copies = depo.ongoing_depostate.depositstatecopies_set.all()

        self.stdout.write("For the given deposit, create as many deposits as publishers.")

        for copy in copies:
            dist = copy.card.distributor
            if dist:
                dep = Deposit.objects.filter(distributor=dist).first()
                if not dep:
                    try:
                        dep_name = "Depot " + dist.name
                    except Exception:
                        pass
                    dep = Deposit.objects.create(name=dep_name,
                                                 distributor=dist)

                try:
                    self.stdout.write("Adding {} to {} x{}...".format(copy.card.title,
                                                                      dep.name,
                                                                      copy.nb_current))
                except Exception:
                    # fucking python unicode support.
                    self.stdout.write("Adding card id {} to deposit {} x{}...".format(
                        copy.card.pk,
                        dep.pk,
                        copy.nb_current))

                # XXX: quantité initiale ?
                dep.add_copy(copy.card, nb=copy.nb_current)
                self.stdout.write(" and removing it from the initial one...")
                depo.remove_card(copy.card.pk)
                dep.save()

        self.stdout.write("All done.")
