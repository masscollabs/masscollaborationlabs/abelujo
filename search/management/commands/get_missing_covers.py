#!/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarez@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

"""
Get the cover URLs of cards that miss it.
"""
from __future__ import print_function
from __future__ import unicode_literals

from django.db.models import Q
from django.core.management.base import BaseCommand

from search.models import Card
from search.views_utils import search_on_data_source_with_cache


def is_isbn(it):
    # XXX: copied from models/utils.py
    """Return True is the given string is an ean or an isbn, i.e:

    - type: str
    - length of 13 or _
    """
    # caution: method duplicate in scraperUtils
    ISBN_ALLOWED_LENGTHS = [13]
    res = False
    if not it:
        return False

    if not len(it) in ISBN_ALLOWED_LENGTHS:
        return False

    if it.startswith('97') or it.startswith('3'):
        return True

    # pattern = re.compile("[0-9]+")
    # if (isinstance(it, six.text_type) or isinstance(it, six.string_types)) and \
    #    pattern.match(it):
    #     res = it

    return res

def cards_without_cover():
    return Card.objects.filter(Q(cover__isnull=True) |
                               Q(cover=''))

def update_card(card, source=""):
    isbn = card.isbn
    # Check ISBN is valid.
    if not is_isbn(card.isbn):
        return False, card, ["ISBN not valid: {}".format(isbn)]

    res, traces = search_on_data_source_with_cache(source, isbn)
    if not res:
        print("{}: no result. Exiting. ('{}')".format(card.isbn, card.title))
        return False, card, traces

    print("{} : ok ({} results)".format(card.isbn, len(res)))

    if len(res) > 1:
        print("INFO: got more than 1 result for {}, we pick the first one.".format(isbn))

    res = res[0]

    try:
        if res['img']:
            card.cover = res['img']
            card.save()
            return True, card, None
        else:
            return False, card, "no cover url found!"

    except Exception as e:
        return False, card, "An error happened: {}".format(e)


class Command(BaseCommand):

    help = "Search covers for cards that miss one (usually the ones coming from Dilicom)."

    def add_arguments(self, parser):
        parser.add_argument(
            '-l',
            dest='lang',
            help='Set the language to better choose the bibliographic source.',
        )

    def print_status(self, msg="Done"):
        self.stdout.write(msg)
        if self.not_found:
            self.stdout.write("ISBNs not found:\n{}".format("\n".join(self.not_found)))
        else:
            self.stdout.write("All ISBNs were found")

    def update_all(self, cards, source=""):
        for card in cards:
            status, card, traces = update_card(card, source=source)
            if status:
                self.cards_updated.append(card)
            else:
                self.cards_missed.append(card)

        return True

    def handle(self, *args, **options):
        """
        """
        if options.get('lang'):
            self.stdout.write("Unimplemented. Would you buy me a beer ?")
            exit(1)

        source = 'librairiedeparis'
        print("INFO: the default bibliographic source is FR. See the (unimplemented) -l option.")

        cards = cards_without_cover()
        cards_with_isbn = []
        for card in cards:
            if is_isbn(card.isbn):
                cards_with_isbn.append(card)
        self.stdout.write("Cards without cover: {}".format(cards.count()))
        self.stdout.write("Cards with valid ISBN: {}".format(len(cards_with_isbn)))
        self.cards_updated = []
        self.cards_missed = []
        try:
            completed = self.update_all(cards_with_isbn, source=source)
            print(completed)
        except KeyboardInterrupt:
            self.stdout.write("User abort.")
        finally:
            self.stdout.write("Cards missed: {} / {}".format(len(self.cards_missed), len(cards_with_isbn)))
            self.stdout.write("Cards updated: {} / {}".format(len(self.cards_updated), len(cards_with_isbn)))
