#!/bin/env python
# -*- coding: utf-8 -*-

"""

"""
from __future__ import print_function
from __future__ import unicode_literals

from tqdm import tqdm

from django.core.management.base import BaseCommand

from search.models import stats
from search.models.utils import to_ascii

import logging
logging.basicConfig(format='%(asctime)s -- %(levelname)s [%(name)s:%(lineno)s]:%(message)s', level=logging.WARNING)


class Command(BaseCommand):

    help = ""

    def add_arguments(self, parser):
        parser.add_argument(
            '-l',
            dest='lang',
            help='Set the language to better choose the bibliographic source.',
        )

    def handle(self, *args, **options):
        """
        """
        if options.get('lang'):
            self.stdout.write("Unimplemented. Would you buy me a beer ?")
            exit(1)

        res = stats.stock_age_state(to_dict_light=False)

        self.stdout.write("stats:")
        from pprint import pprint
        for age, data in res.items():
            self.stdout.write("age: {}".format(age))
            pprint(data)
            # self.stdout.write("{}".format(data))
