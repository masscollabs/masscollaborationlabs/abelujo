#!/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarez@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

"""
Create a number of sells, in order to populate the history.

To be run from a cron job at the end of the day, ±22h.
We create sells with a random hour starting at about -12.

./manage.py random_sells

"""


import datetime
import os
import random

import pendulum
from django.utils import timezone
from django.core.management.base import BaseCommand

from abelujo import settings
from search.models import Card
from search.models import Sell
from search.models.common import PAYMENT_CHOICES

# py2/3
try:
    input = raw_input
except NameError:
    pass

NUMBER_SELLS = 17
DATE_FORMAT = "%Y-%m-%d %H:%M:%S"

class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('-n',
                            dest="n",
                            action="store",
                            help="number of sells")

    def handle(self, *args, **options):

        number_sells = options.get('n')
        if number_sells:
            number_sells = int(number_sells)
        else:
            number_sells = NUMBER_SELLS
            # Slightly change the number of sells
            delta = random.randrange(0, 5, 1)
            number_sells += delta

        # now = timezone.now()
        now = pendulum.now()

        h_start = 9
        h_delta = 0
        m_start = 1
        m_delta = 0

        max_card_id = Card.objects.last().pk

        payments = PAYMENT_CHOICES
        payments_ids = [it[0] for it in payments]

        for i in xrange(number_sells):
            h_delta = random.randrange(3, 12, 1)  # depends on when we run the cron job.
            m_delta = random.randrange(1, 59, 1)
            delta = datetime.timedelta(hours=h_delta * -1, minutes=m_delta)
            hour = now.add_timedelta(delta)

            nb_books = random.randrange(1, 5, 1)
            to_sell = []  # list of dicts: id, price_sold, quantity
            total_payment = 0
            cards = []
            for j in range(nb_books):
                card_id = random.randrange(1, max_card_id, 1)
                card = Card.objects.filter(pk=card_id).first()
                if card:
                    cards.append(card)
                    quantity = random.randrange(1, 3, 1)
                    to_sell.append({
                        "id": card_id,
                        "price_sold": card.price,
                        "quantity": quantity,
                    })
                    if card.quantity < 1:
                        card.add_card(nb=(card.quantity * -1 + 1))

            def get_payment_id():
                idx = random.randrange(0, 1, 1)
                # We don't want payments that don't count in the total of the day,
                # like "bons d'achats", it's only confusing for newcomers (and me).
                #
                # We pick only one of the two first declared payment means.
                return idx

            idx = get_payment_id()
            payment_id = payments_ids[idx]

            total_payment = sum([it["price_sold"] * it["quantity"] for it in to_sell])

            print("Selling {}/{}…".format(i, number_sells))
            date = datetime.datetime(
                hour.year,
                hour.month,
                hour.day,
                hour.hour,
                hour.minute,
            )
            date = datetime.datetime.strftime(date, DATE_FORMAT)

            sell, status, msgs = Sell.sell_cards(
                to_sell,
                date=date,
                payment=payment_id,
                total_payment_1=total_payment,
            )
            if not sell:
                print("random_sells: sell was not successful: {}".format(msgs))
