# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarez@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

"""
OVerride the xgettext command to be able to collect double underscores as translatable strings, to appear in the .po file.

We use ugettext as _ AND ugettext_lazy as __ but the default gettext only collects _("…") strings. We want to collect all. For the admin pages!

https://docs.djangoproject.com/en/dev/topics/i18n/translation/#customizing-the-makemessages-command

"""
from django.core.management.commands import makemessages

class Command(makemessages.Command):
    print("----> Using our custom makemessages command to collect both '_' and double '__' (gettext and gettext_lazy), in models.py, for the Admin panel.")
    xgettext_options = makemessages.Command.xgettext_options + ['--keyword=__']
