#!/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarez@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

"""
Compute the quantities and save to a DB field.

./manage.py compute_quantities

Takes around 3 minutes for a thousand objects.

"""
from __future__ import unicode_literals

from django.core.management.base import BaseCommand

from search.models import Place

# py2/3
try:
    input = raw_input
except NameError:
    pass


class Command(BaseCommand):

    def handle(self, *args, **options):
        self.stdout.write("Go...")
        places = Place.objects.all()
        # self.stdout.write("All existing places: {}".format(Place.objects.all()))
        self.stdout.write("Computing the discounted cost of {} places?".format(len(places)))
        confirmation = raw_input("Continue ? [Y/n]")
        if confirmation == "n":
            exit(0)

        for place in places:
            self.stdout.write("Place: {}".format(place))
            cost_discounted = place.cost_discounted()
            cost_discounted_excl_vat = place.cost_discounted_excl_vat()
            self.stdout.write("-------------------")
            self.stdout.write("Discounted cost: {}".format(cost_discounted))
            self.stdout.write("Discounted cost, exc VAT: {}".format(cost_discounted_excl_vat))
            self.stdout.write("\n")
