# -*- coding: utf-8 -*-
# Copyright (c) Vincent Dardel, <vindarez@mailz.org>,  Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

"""
Stripe payments OR simple book reservations without payments.

The website first calls /api/checkout => api_stripe which does some checks and calls handle_api_stripe

If it's a payment, then Stripe needs to call web hooks => api_stripe_hooks
"""

from __future__ import unicode_literals

import httplib
import json
import logging

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from pprint import pprint

from abelujo import settings
from search import mailer
from search.models import Card
from search.models import Client
from search.models import Reservation
from search.models.utils import _is_truthy
from search.models.utils import get_logger
from search.models.utils import get_total_weight

from search.views_utils import bulk_import_from_dilicom
# from search.views_utils import search_on_data_source_with_cache  # replaced by bulk search.

try:
    import stripe
    stripe.api_key = settings.STRIPE_SECRET_API_KEY
except ImportError:
    pass

logging.basicConfig(format='%(asctime)s -- %(levelname)s [%(name)s:%(lineno)s]:%(message)s', level=logging.DEBUG)
log = get_logger()


def create_checkout_session(abelujo_payload):
    """
    Create a session object on Stripe.
    It returns a JSON. Our identifiers can be its ID or the payment_intent ID. We will find them back on the webhook.

    Mocked in unit tests.
    """
    if settings.STRIPE_SECRET_API_KEY:
        assert stripe.api_key
    else:
        return
    session = None
    try:
        payload = abelujo_payload.get('order').get('stripe_payload')
        if payload:
            session = stripe.checkout.Session.create(
                success_url=payload.get('success_url'),
                cancel_url=payload.get('cancel_url'),
                payment_method_types=payload.get('payment_method_types'),
                line_items=payload.get('line_items'),
                mode=payload.get('mode'),
            )
        else:
            log.warning('No payload found in:\n{}'.format(abelujo_payload))
    except Exception as e:
        log.error("stripe.create_checkout_session: could not create checkout session. Full payload: {}".format(abelujo_payload))
    return session


#
# Response:
#
# <Session checkout.session id=cs_test_a1YB0YtTZwCDbyyKVHOORALKGlYAPMIIHUEByGSWgnzO9a2yp5T4YFWgUW at 0x7f12dc0da180> JSON: {
#   "allow_promotion_codes": null,
#   "amount_subtotal": 200,
#   "amount_total": 200,
#   "automatic_tax": {
#     "enabled": false,
#     "status": null
#   },
#   "billing_address_collection": null,
#   "cancel_url": "https://example.com/cancel",
#   "client_reference_id": null,
#   "currency": "eur",
#   "customer": null,
#   "customer_details": null,
#   "customer_email": null,
#   "display_items": [
#     {
#       "amount": 100,
#       "currency": "eur",
#       "custom": {
#         "description": null,
#         "images": null,
#         "name": "lineitem1"
#       },
#       "quantity": 2,
#       "type": "custom"
#     }
#   ],
#   "id": "cs_test_a1YB0YtTZwCDbyyKVHOORALKGlYAPMIIHUEByGSWgnzO9a2yp5T4YFWgUW",
#   "livemode": false,
#   "locale": null,
#   "metadata": {},
#   "mode": "payment",
#   "object": "checkout.session",
#   "payment_intent": "pi_1JEwbgJqOLQjpdKj4T1cdrI3",  # returned also in webhook
#   "payment_method_options": {},
#   "payment_method_types": [
#     "card"
#   ],
#   "payment_status": "unpaid",
#   "setup_intent": null,
#   "shipping": null,
#   "shipping_address_collection": null,
#   "submit_type": null,
#   "subscription": null,
#   "success_url": "https://example.com/success",
#   "total_details": {
#     "amount_discount": 0,
#     "amount_shipping": 0,
#     "amount_tax": 0
#   },
#   "url": "https://checkout.stripe.com/pay/cs_test_a1YB0YtTZwCDbyyKVHOORALKGlYAPMIIHUEByGSWgnzO9a2yp5T4YFWgUW#fidkdWxOYHwnPyd1blpxYHZxWk9TQHNxSEFsbGN0YnVvdFFJNjF1MkFJNTU1dEJ8cjQ2QE0nKSdjd2poVmB3c2B3Jz9xd3BgKSdpZHxqcHFRfHVgJz8ndmxrYmlgWmxxYGgnKSdga2RnaWBVaWRmYG1qaWFgd3YnP3F3cGB4JSUl"
# }


def _ensure_ascii_email(to_email):
    """
    Ensure this email address is ascii. pysendgrid is very picky (grrrr).
    """
    if to_email:
        try:
            to_email = to_email.encode('ascii', 'ignore')
            return to_email
        except Exception as e:
            log.warning("api_stripe: error trying to encode the email address {} to ascii: {}".format(to_email, e))
            return ""

def _do_handle_api_stripe(payload):
    """
    From this payload (dict), find or create the client, the reservation of the cards,

    If it's an online payment, create a stripe session and only pre-sell the reservations.
    They have to be validated in the webhook.

    If it is NOT an online payment but a reservation of books without payment, proceed.

    We can buy cards we have in stock, or not. In that case, we bulk
    search the ISBNs with Dilicom (1 query with as many ISBNs as we
    get), we create the cards on DB and we add them to the auto-command list.

    Important keys:
    - buyer.billing_address, buyer.shipping_address
    - order.stripe_payload (optional)

    Return: dict (data, status, alerts).
    """
    res = {'data': "",
           'alerts': [],
           'status': httplib.OK,
           }
    buyer = payload.get('buyer')
    order = payload.get('order')

    # Is is an online payment?
    is_online_payment = False

    # Is Stripe enabled in our settings? (we need a private api key and a webhook secret)
    stripe_enabled = False

    ###################
    ## Basic checks. ##
    ###################
    # Is Stripe enabled in our settings?
    if not settings.STRIPE_SECRET_API_KEY:
        # Don't fail unit tests on CI.
        # I don't feel like installing the Stripe python lib by default…
        stripe_enabled = False
    else:
        stripe_enabled = True

    if not order:
        # order can just be "retrait sur place" / "take up book at the store"
        msg = "We are asked to process a payment with no order, that's not possible. payload: {}".format(payload)
        log.warning(msg)
        res['alerts'].append(msg)
        res['status'] = 400
        return res

    if not buyer:
        log.warning('We are asked to process a payment with no buyer, strange! payload is {}'.format(payload))

    ############################
    ## Handle Stripe session. ##
    ############################
    session = {}
    is_online_payment = payload.get('order', {}).get('online_payment')  # true or false
    is_online_payment = _is_truthy(is_online_payment)

    if is_online_payment and not stripe_enabled:
        log.warning("api_stripe: you are trying to send a payment with Stripe but settings.STRIPE_SECRET_API_KEY is NOT set.")
        res['status'] = 404
        res['alerts'].append("Authentication error. Trying to process an online payment, but we miss Stripe settings.")
        return res

    if is_online_payment:
        try:
            if payload.get('order').get('stripe_payload'):
                log.info("Processing Stripe payload...")
                session = create_checkout_session(payload)
                res['data'] = session
            else:
                log.info("No Stripe payload to process.")
            # return JsonResponse(session)
        except Exception as e:
            res['alerts'].append("{}".format(e))
            res['status'] = httplib.INTERNAL_SERVER_ERROR

    else:
        log.info("processing... (this is NOT an online payment).")

    ################################################
    ## Create clients, commands.
    ## If online payment, they are not validated yet.
    ################################################
    billing_address = buyer.get('billing_address')
    # delivery_address = buyer.get('delivery_address')

    existing_client = Client.objects.filter(email=billing_address.get('email')).first()
    # TODO: update existing client.
    if existing_client:
        log.info("client {} already exists.".format(existing_client))
    else:
        try:
            existing_client = Client(
                name=billing_address.get('last_name').strip(),
                firstname=billing_address.get('first_name').strip(),
                email=billing_address.get('email').strip(),
                city=billing_address.get('city').strip(),
                country=billing_address.get('country').strip(),
                zip_code=billing_address.get('postcode').strip(),
                address1=billing_address.get('address').strip(),
                address2=billing_address.get('address_comp').strip(),
                telephone=billing_address.get('phone').strip(),
            )
            existing_client.save()
        except Exception as e:
            log.error("This new client ({}) could not be created in DB: {}. billing_address is: {}".format(billing_address.get('name'), e, billing_address))
            res['alerts'].append('Error creating a new client')
            res['status'] = 500

    #####################################################################
    # Get the books we want to sell.
    # 1. in abelujo_items: we have those ones in stock.
    # 2. in search_items, we do NOT have them in stock (they were fetched
    # from a datasource provide such as Electre) and we need to get them.
    #####################################################################
    # 1. existing books.
    # cards we sell: list of 'id' and 'qty'.
    ids_qties = order.get('abelujo_items')
    # cards_ids = [it.get('id') for it in cards_qties]
    # cards, msgs = Card.get_from_id_list(cards_ids)
    # if msgs:
    #     res['alerts'].append(msgs)

    cards_qties = []  # all cards and quantities, to reserve for the client.
    cards = []  # all cards (finally unused?).

    for id_qty in ids_qties:
        card = Card.objects.filter(pk=id_qty.get('id')).first()
        if card:
            cards_qties.append({'card': card, 'qty': id_qty.get('qty')})
            cards.append(card)
        else:
            log.warning("Selling cards with Stripe, we could not find a card: {}".format(id_qty.get('id')))

    # 2. new books.
    # We get a list of dicts with: isbn, quantity, but also price and publisher
    # in the case we need to disambiguate an ISBN search that returns multiple results
    # (yes that is possible, ISBNs are NOT unique, a book might be in two different editions).
    search_items = order.get('search_items')
    # new_cards = []  # list of dicts with card and qty like above.
    if search_items:
        isbns = [it.get('isbn') for it in search_items]
        if not isbns:
            logging.warning("stripe checkout: we were asking to command new books, but we get no ISBNs!")
            pass

        # Bulk search on Dilicom.
        dilicom_results, messages = bulk_import_from_dilicom(isbns)
        if len(dilicom_results) != len(isbns):
            logging.warning("stripe checkout: we had to find data for {} ISBNs ({}) but we found data for {} book(s): {}. Messages: {}".format(
                len(isbns),
                isbns,
                len(dilicom_results),
                dilicom_results, messages
            ))

        for item in search_items:
            # Double-check we still don't have it in stock.
            existing = Card.objects.filter(isbn=item.get('isbn')).first()
            if existing:
                cards_qties.append({'card': existing, 'qty': item.get('quantity')})
                cards.append(existing)
                if existing.quantity <= 0:
                    existing.add_to_auto_command(nb=item.get('quantity'))
                continue

            isbn = item.get('isbn')

            # First error handling.
            if not isbn:
                logging.warning("stripe checkout: We wanted to search for a book we don't have in stock, but we got a bad ISBN: {}".format(isbn))
                # Probably it is OK if we continue here?
                continue

            # Get result from previous Dilicom search.
            results = filter(lambda it: it.get('isbn') == isbn, dilicom_results)
            # Otherwise, dumb way to search on Electre (only 1 ISBN query at a time):
            # results, messages = search_on_data_source_with_cache('electre', isbn)
            if not results:
                logging.warning("stripe checkout: we needed to look for ISBN {} on Electre, but couldn't get a result. The reservation can not be confirmed. messages: {}. Result was: {}".format(item.get('isbn'), messages, results))
                raise Exception("stripe checkout error: no book found for ISBN '{}'".format(isbn))

            # OK, save result in DB.
            existing, _ = Card.from_dict(results[0])
            # XXX: case of multiple results for same ISBN :/
            qty = item.get('quantity')
            if qty is None:
                logging.warning('api_stripe: we couldnt find the item quantity, so it defaults to 1. item: {}'.format(item))
                qty = 1
            cards_qties.append({
                'card': existing,
                'qty': qty,
            })
            cards.append(existing)

            # Add to the command list.
            existing.add_to_auto_command(nb=item.get('quantity'))

    ##################################################
    # Test configuration?
    # If the client is the developer, we are testing.
    ##################################################
    its_only_a_test = False
    client_email = billing_address.get('email')
    client_lastname = billing_address.get('last_name').strip()
    client_firstname = billing_address.get('first_name').strip()
    if client_email == settings.TEST_EMAIL_BOOKSHOP_RECIPIENT \
       or client_lastname == settings.TEST_LASTNAME \
       or client_firstname == settings.TEST_FIRSTNAME:
        its_only_a_test = True

    ##################################################
    # Reserve.
    # (if it's not a live test by the developer)
    ##################################################
    # PERFORMANCE:
    reservations = []  # should be one for all...
    try:
        payment_intent = None
        if not is_online_payment:
            is_paid = False
            is_ready = True
        else:
            # is_paid = True
            is_paid = False  # needs to be validated in the webhook.
            payment_intent = session.get('payment_intent')
            is_ready = False

        send_by_post = True
        if order.get('shipping_method') and "retrait" in order.get('shipping_method').lower():
            send_by_post = False

        payment_meta = payload
        try:
            payment_meta = json.dumps(payload)
        except Exception as e:
            log.warning("stripe api: we try to encode payload to JSON to store it in payment_meta, but that failed: {}".format(e))

        if True:  # can use its_only_a_test for testing.
            for card_qty in cards_qties:
                resa, created = existing_client.reserve(card_qty.get('card'),
                                                        nb=card_qty.get('qty'),
                                                        send_by_post=send_by_post,
                                                        is_paid=is_paid,
                                                        is_ready=is_ready,
                                                        payment_origin="stripe",
                                                        payment_meta=payment_meta,
                                                        payment_session=session,
                                                        payment_intent=payment_intent)
                if resa:
                    reservations.append(resa)
            res['alerts'].append("client commands created successfully")

    except Exception as e:
        log.error("Error creating reservations for {}: {}".format(cards_qties, e))

    ##############################################
    ## Send confirmation email to both parties. ##
    ##############################################

    # Cleanup optional mondial relay data (from string/JSON to dict).
    payload = parse_mondial_relay_json_string(payload)

    # If the client is the tester (me), send him the email with the ongoing template.
    if not its_only_a_test:  # DEV: for now, get the emails to check the layout.
        mail_sent = mailer.send_client_command_confirmation(cards=cards,
                                                            to_emails=settings.TEST_EMAIL_BOOKSHOP_RECIPIENT,
                                                            payload=payload,
                                                            payment_meta=payload,
                                                            reply_to=settings.EMAIL_BOOKSHOP_RECIPIENT,
                                                            # added:
                                                            use_theme=True)
        log.warning("We send a confirmation email to the tester. Mail sent? {}, payload: {}".format(mail_sent, payload))
        if mail_sent:
            status = 200
        else:
            status = 500
        res['status'] = status

    log.info('is this an online payment ? {}'.format(is_online_payment))  # XXX:
    if not is_online_payment and not its_only_a_test:
        # Send confirmation to client.
        to_email = None
        if existing_client and existing_client.email:
            to_email = existing_client.email
        # Defensive. Ensure the email adress is ascii.
        if to_email:
            to_email = _ensure_ascii_email(to_email)

        try:
            mail_sent = mailer.send_client_command_confirmation(cards=cards,
                                                                to_emails=to_email,
                                                                payload=payload,  # unused...
                                                                payment_meta=payload,
                                                                reply_to=settings.EMAIL_BOOKSHOP_RECIPIENT,
                                                                # theme!
                                                                use_theme=True)
            log.info("stripe: confirmation email sent to client ? {} (not an online payment). Payload: {}".format(mail_sent, payload))
            if not mail_sent:
                log.warning("stripe: confirmation email to client (not an online payment) was not sent :S")
                pass  # TODO: register info in reservation.
        except Exception as e:
            log.error("stripe: could not send confirmation email (not an online payment): {}".format(e))

        # Send confirmation to bookshop owner.
        if settings.EMAIL_BOOKSHOP_RECIPIENT:
            # Get total weight of cards.
            total_weight, weight_message = get_total_weight(cards)
            try:
                owner_email = settings.EMAIL_BOOKSHOP_RECIPIENT
                if its_only_a_test:
                    owner_email = settings.TEST_EMAIL_BOOKSHOP_RECIPIENT
                log.info("stripe: is it a test? {} owner email: {}".format(
                    its_only_a_test, owner_email))
                mail_sent = mailer.send_owner_confirmation(cards=cards,
                                                           payload=payload,
                                                           payment_meta=payload,
                                                           total_weight=total_weight,
                                                           weight_message=weight_message,
                                                           is_online_payment=is_online_payment,
                                                           email=owner_email,
                                                           owner_name=settings.BOOKSHOP_OWNER_NAME,
                                               )
                log.info("stripe, reservation but no payment: confirmation sent to owner: {} ? {}".format(owner_email, mail_sent))
                if not mail_sent:
                    log.warning("stripe: confirmation email to owner (not an online payment) was not sent :S")
            except Exception as e:
                log.error("api_stripe: could not send confirmation email to owner: {} (not a payment)".format(e))
        else:
            log.warning("stripe: sell with payment intent {} was successfull and we wanted to send an email to the bookshop owner, but we can't find its email in settings.".format(payment_intent))

    return res

def handle_api_stripe(payload):
    try:
        return _do_handle_api_stripe(payload)
    except Exception as e:
        log.error("stripe.handle_api_stripe: {}".format(e))
        return {'data': "",
                'alerts': [],
                'status': httplib.ERROR,
                }


def _do_parse_mondial_relay_json_string(real_test_payload):
    """
    Parse mondial relay data if it exists. (it's JSON in another string).
    If it doesn't exist, it's alright, return the payload as is.
    Return: the payload (dict) with a dict instead of the string.
    """
    if real_test_payload is None:
        log.warning('parsing mondial relay data, but this payload is None.')
    try:
        mondial_relay_json = real_test_payload['order']['mondial_relay_AP']
        mondial_relay_dict = json.loads(mondial_relay_json)
        real_test_payload['order']['mondial_relay_AP'] = mondial_relay_dict
        log.info('We cleaned up the Mondial Relay data from string to JSON')
    except Exception as e:
        log.info('Could not parse order.mondial_relay_AP JSON data from the following payload, so there is no cleanup to do. If it is not a relay command, everything is OK! {}: {}'.format(real_test_payload, e))

    return real_test_payload

def parse_mondial_relay_json_string(real_test_payload):
    try:
        return _do_parse_mondial_relay_json_string(real_test_payload)
    except Exception as e:
        log.error("stripe.parse_mondial_relay_json_string: {}".format(e))


def _do_api_stripe(request, **response_kwargs):
    """
    API entry point of the Stripe handling machinery. The frontend calls here.

    We can buy books with Stripe, or just "reserve" books without an
    online payment. In both cases, the frontend calls here.

    If we receive a POST request, then call `handle_api_stripe` to do
    the job.

    Return: a JsonResponse to return to the web client.
    """
    # NB: we used to have more checks here, but now this is a just a
    # thin layer on top of the api handler.
    res = {'data': "",
           'alerts': [],
           'status': httplib.OK,
           }
    payload = {}
    # status = 200
    if request.method == 'POST':
        if request.body:
            payload = json.loads(request.body)
            try:
                res = handle_api_stripe(payload)
            except Exception as e:
                log.error("Error handling the api stripe payment: {}. Payload used: {}".format(e, payload))
                res['status'] = 500
                res['alerts'].append("Error handling the api stripe payment.")
                import inspect  # debug
                pprint(inspect.trace())

        return JsonResponse(res)

    else:
        return JsonResponse({'alerts': ['Use a POST request']})

def api_stripe(request, **response_kwargs):
    try:
        return _do_api_stripe(request, **response_kwargs)
    except Exception as e:
        log.error("stripe.api_stripe: {}".format(e))
        return JsonResponse({'alerts': ['Internal error']}, status=500)


def _stripe_construct_event(payload, signature, webhook_secret):
    """
    Helper to construct a Stripe event object.

    Return: a stripe event object.
    """
    event = stripe.Webhook.construct_event(
        payload, signature, webhook_secret
    )
    return event

@csrf_exempt
def _do_api_stripe_hooks(request, **response_kwargs):
    """
    API endpoint to handle post-payment webhooks and validate the payment.
    https://stripe.com/docs/payments/handling-payment-events

    Find the pre-saved reservations with the payment_intent ID and validate them.

    Then, send validation emails to the client and the bookshop.

    Return: a JsonResponse (to Stripe).
    """
    if not settings.STRIPE_SECRET_API_KEY:
        # Don't fail unit tests on CI.
        return

    # Mocking stripe functions, request object and having the right payloads
    # is not so easy, so let's use is_test...
    is_test = response_kwargs.get('is_test')
    if not is_test:
        payload = request.body
        signature = request.META.get('HTTP_STRIPE_SIGNATURE')
    webhook_secret = settings.STRIPE_WEBHOOK_SECRET

    ongoing_reservations = []
    cards = []
    event = None
    res = {'data': "",
           'alerts': [],
           'status': httplib.OK,
           }
    sell_successful = None

    # Build a Stripe event object from the payload.
    event = {}
    try:
        # stripe.event.construct_from(
        #   json.loads(payload), stripe.api_key
        # )
        if not is_test:
            event = _stripe_construct_event(payload, signature, webhook_secret)
    except stripe.error.SignatureVerificationError as e:
        res['alerts'].append('Invalid Stripe signature')
        return JsonResponse(res, status=400)
    except ValueError as e:
        # Invalid payload
        res['alerts'].append("{}".format(e))
        return JsonResponse(res, status=400)

    # Handle the event.
    # We consider the sell successfull on receiving the session.completed event.
    if is_test:
        sell_successful = True
    if event and event.get('type') == 'checkout.session.completed':
        # We consider the payment was succesfull only with this event.
        sell_successful = True
        log.info("stripe hook: received checkout.session.completed signal. Now looking for the payment intent id.")
    # ... handle other event types
    else:
        log.info('Unhandled event type {}'.format(event.get('type')))
        res['alerts'].append('Unhandled event type: {}'.format(event.get('type')))
        return JsonResponse(res, status=200)

    # If the sell was not successful, warn the bookshop.
    # ... or let Stripe handle it.
    if not sell_successful:
        return JsonResponse(res, status=200)

    #
    # Look for the payment intent ID.
    #
    payload = json.loads(request.body)
    payment_intent = None
    pprint(payload)
    if sell_successful:
        try:
            payment_intent = payload['data']['object']['payment_intent']
            log.info("stripe hook: found payment intent: {}. Everything goes well.".format(payment_intent))
        except Exception as e:
            log.error("Could not get the payment intent ID in in webhook (in data.object.payment_intent). Let me search in charges.0.data now. {}".format(e))

        if not payment_intent:
            try:
                payload_data = payload['data']['object']['charges']['data']
                if payload_data and len(payload_data):
                    payment_intent = payload_data[0].get('payment_intent')
                    log.info("stripe hook: found payment intent inside charges.data.0: {}. Payload: {}".format(payment_intent, payload))

                if len(payload_data) > 1:
                    log.info("stripe hook: payload's charges has more than 1 element and we don't handle it: {}".format(payload_data))
            except Exception as e:
                log.warning("stripe hook: looking for the payment_intent failed: it was not in data.object.payment_intent and we don't find it in data.object.charges.data.0 either. {}".format(e))

    #
    # If no payment ID found, log, but send an email to the owner anyways
    # (so she knows something happened).
    #
    if not payment_intent:
        log.warning("stripe hook: we did not find the payment_intent, so we won't validate reservations or send emails. {}".format(e))

    #
    # Confirm the reservations.
    #
    ongoing_reservations = None
    payment_meta = None  # Data from the first POST: buyer.billing_address, order.amount etc.
    if payment_intent:
        ongoing_reservations = Reservation.objects.filter(payment_intent=payment_intent)
    # So we have several reservation objects, but they should be of the same client and
    # part of the same command...
    client = None
    # send_by_post = False
    if ongoing_reservations:
        client = ongoing_reservations.first().client
        log.info("stripe hook: found client: {}".format(client))
        # send_by_post = ongoing_reservations.first().send_by_post
        cards = [it.card for it in ongoing_reservations]
        payment_meta = ongoing_reservations.first().payment_meta  # text field
        log.info("stripe hook: found payment meta data: {}".format(payment_meta))
        try:
            payment_meta = json.loads(payment_meta)
            log.info("payment_meta decoded from json")
        except Exception as e:
            log.warning("stripe hook: we are trying to decode the payment_meta from a JSON string, but it failed. We maybe have data from before sept, 14th. {}".format(e))
            payment_meta = None  # prevent decoding errors in templates (?).

    else:
        log.warning("stripe webhook: we didn't find any ongoing reservation with payment intent {}. We won't be able to find a related client and to send confirmation emails.".format(payment_intent))

    # Quickly check we have the same clients.
    if client:
        for resa in ongoing_reservations:
            if resa.client != client:
                log.warning("mmh we have ongoing reservations with payment_intent {} but not the same client ?? {} / {}".format(payment_intent, client, resa.client))

    ###############################
    ## Send confirmation emails. ##
    ###############################
    to_email = ""
    # amount = None
    # amount_fmt = ""
    # currency = ""
    # currency_symbol = "EUR"
    description = ""
    is_stripe_cli_test = False
    cli_test_sign = "(created by Stripe CLI)"
    if sell_successful:
        # Now, validate the reservations.
        # for resa in ongoing_reservations:
        #     resa.is_paid = True
        #     resa.save()
        if ongoing_reservations:
            ongoing_reservations.update(is_paid=True, is_ready=True)
        # ongoing_reservations.update(is_ready=True)

        # Get emails.
        if client:
            to_email = client.email if client else ""

        try:
            description = payload['data']['object']['charges']['data'][0]['description']
            if description and cli_test_sign in description:
                is_stripe_cli_test = True
        except Exception as e:
            log.info("api_stripe_hooks: could not get the description, so we are probably NOT in a stripe cli test but in production. The exception is: {}".format(e))

        #
        # Cleanup optional mondial relay data (from string/JSON to dict).
        #
        payload = parse_mondial_relay_json_string(payload)
        payment_meta = parse_mondial_relay_json_string(payment_meta)

        # Ensure ascii for python Stripe library... shame on you.
        if is_stripe_cli_test:
            to_email = "".join(list(reversed('gro.zliam@leradniv')))  # me@vindarel
        if to_email:
            to_email = _ensure_ascii_email(to_email)

        # Send it, damn it.
        try:
            if to_email:
                mail_sent = mailer.send_client_command_confirmation(cards=cards,
                                                                    # total_price=amount_fmt,
                                                                    to_emails=to_email,
                                                                    payload=payload,
                                                                    payment_meta=payment_meta,
                                                                    is_online_payment=True,
                                                                    reply_to=settings.EMAIL_BOOKSHOP_RECIPIENT,
                                                                    # theme!
                                                                    use_theme=True)
                log.info("stripe webhook: confirmation mail sent to {} ? {}".format(to_email, mail_sent))

                if not mail_sent:
                    # TODO: register info in reservation.
                    log.warning("stripe hook: payment confirmation email to client was not sent :S")
        except Exception as e:
            log.error("api_stripe: could not send confirmation email: {}".format(e))

        # Send the same email to the developer, to check theme rendering.
        try:
            mail_sent = mailer.send_client_command_confirmation(cards=cards,
                                                                is_online_payment=True,
                                                        to_emails=settings.TEST_EMAIL_BOOKSHOP_RECIPIENT,
                                                        payload=payload,
                                                        payment_meta=payment_meta,
                                                        reply_to=settings.EMAIL_BOOKSHOP_RECIPIENT,
                                                        # added: use custom theme.
                                                        use_theme=True)
            log.info("stripe webhook: confirmation mail sent to {} ? {}".format(settings.TEST_EMAIL_BOOKSHOP_RECIPIENT, mail_sent))
        except Exception as e:
            log.warn("api_stripe hook: could not send email with custom theme to developer: {}".format(e))

        #
        # Send confirmation to bookshop owner.
        #
        if settings.EMAIL_BOOKSHOP_RECIPIENT:
            # Get total weight of cards.
            total_weight, weight_message = get_total_weight(cards)
            try:
                mail_sent = mailer.send_owner_confirmation(cards=cards,
                                                           total_weight=total_weight,
                                                           weight_message=weight_message,
                                                           payload=payload,
                                                           payment_meta=payment_meta,
                                                           is_online_payment=True,
                                                           client=client,
                                                           # total_price=amount_fmt,
                                                           email=settings.EMAIL_BOOKSHOP_RECIPIENT,
                                                           owner_name=settings.BOOKSHOP_OWNER_NAME,
                                                           )
                log.info("stripe webhook: confirmation sent to owner: {} ? {}".format(settings.EMAIL_BOOKSHOP_RECIPIENT, mail_sent))
                if not mail_sent:
                    log.warning("stripe hook: payment confirmation email to client was not sent :S")
            except Exception as e:
                log.error("api_stripe webhook: could not send confirmation email to owner: {}".format(e))
        else:
            log.warning("stripe webhook: sell with payment intent {} was successfull and we wanted to send an email to the bookshop owner, but we can't find its email in settings.".format(payment_intent))

    # Sell not validated.
    else:
        pass

    return JsonResponse(res, status=200)

def api_stripe_hooks(request, **response_kwargs):
    try:
        return _do_api_stripe_hooks(request, **response_kwargs)
    except Exception as e:
        log.error("stripe.api_stripe_hooks: {}".format(e))
        return JsonResponse({'alerts': ['Internal error']}, status=500)
