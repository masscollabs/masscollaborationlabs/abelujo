# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0059_auto_20220428_1541'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='is_institution',
            field=models.BooleanField(default=False, verbose_name='Is this client an institution?'),
        ),
    ]
