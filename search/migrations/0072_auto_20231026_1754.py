# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0071_basket_bon_de_commande'),
    ]

    operations = [
        # migrations.AddField(
        #     model_name='outmovement',
        #     name='institution_basket',
        #     field=models.ForeignKey(related_name='institution_basket', blank=True, to='search.Basket', null=True),
        # ),
        migrations.AlterField(
            model_name='outmovement',
            name='typ',
            field=models.IntegerField(choices=[(1, 'sell'), (2, 'return'), (3, 'loss'), (4, 'gift'), (5, 'box'), (6, 'institution')]),
        ),
    ]
