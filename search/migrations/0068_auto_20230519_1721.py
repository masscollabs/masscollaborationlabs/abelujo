# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import print_function

from django.db import models, migrations
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible

from search.models import Card

from tqdm import tqdm

"""
Data migration to follow the previous schema migration.

We added card.last_soldcard_date, we now save the value for all cards.

If this causes an issue in the future, just comment out the code.
"""

# Keep in mind when testing: if the migration fails (ipython exited with C-c or C-d too),
# the DB is not modified ;)

def noop(app, schema_editor):
    # nothing special to migrate backwards.
    pass

def save_all_last_soldcard_date(app, schema_editor):
    cards = Card.objects.filter(last_soldcard_date__isnull=True)
    # cards = Card.objects.filter(last_soldcard_date__isnull=True)[:10]  # test

    count = len(cards)
    print("Total cards: {}".format(Card.objects.count()))
    print("Remaining cards to set: {}".format(count))

    if not cards:
        # or tqdm will error out with None.
        print("No cards, nothing to do.")
        # exit(0)  # never exit, nor with 0 nor 1.
        return

    start = timezone.now()

    for i, card in tqdm(enumerate(cards)):
        soldcard = card.soldcards_set.last()
        if soldcard:
            date = soldcard.created
            txt = "[{}/{}] last soldcard for {}: {}".format(
                      i,
                      count,
                      card.pk,
                      date,
            )
            # print(txt, end="")
            soldcard.save()

            if card.last_soldcard_date != date:
                print("uuuuh dates don't match? For {} & {}".format(card, date))
                # exit(1)  # NEVER! the migrations won't continue, the DB won't be nicely initialized.

            # print("…ok")
            # print("V", end="")
        else:
            # print("[{}/{}]: card {}: never sold.".format(i, count, card.pk))
            # print(".", end="")
            pass

    end = timezone.now()
    print("All done (in {}s).".format(end - start))

class Migration(migrations.Migration):


    dependencies = [
        ('search', '0067_auto_20230519_1708'),
    ]

    operations = [
        migrations.RunPython(save_all_last_soldcard_date, noop),
    ]
