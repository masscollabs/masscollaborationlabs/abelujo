# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0049_auto_20211211_1200'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContactSMS',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('body', models.TextField(max_length=10000, null=True, verbose_name='SMS body', blank=True)),
                ('data', models.TextField(max_length=10000, null=True, verbose_name='SMS body', blank=True)),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='search.Client', null=True)),
            ],
            options={
                'ordering': ('created',),
            },
        ),
        migrations.AddField(
            model_name='bookshop',
            name='sms_history',
            field=models.ManyToManyField(related_name='search_bookshop_related', verbose_name='History of text messages sent to this contact (SMS)', to='search.ContactSMS', blank=True),
        ),
        migrations.AddField(
            model_name='client',
            name='sms_history',
            field=models.ManyToManyField(related_name='search_client_related', verbose_name='History of text messages sent to this contact (SMS)', to='search.ContactSMS', blank=True),
        ),
    ]
