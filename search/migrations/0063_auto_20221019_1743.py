# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0062_auto_20220902_1133'),
    ]

    operations = [
        migrations.AddField(
            model_name='basket',
            name='is_supplier_return',
            field=models.BooleanField(default=False, verbose_name='Automatically created for every supplier we have books from. Behaves like a box.'),
        ),
    ]
