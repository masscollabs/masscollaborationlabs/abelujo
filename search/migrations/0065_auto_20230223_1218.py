# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0064_auto_20221118_1455'),
    ]

    operations = [
        migrations.CreateModel(
            name='InstitutionBasket',
            fields=[
                ('basket_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='search.Basket')),
                ('raw_data', models.CharField(max_length=200, null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('search.basket',),
        ),
        migrations.AddField(
            model_name='client',
            name='cmdcollectivites_uuid',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='author',
            name='name_ascii',
            field=models.CharField(max_length=200, null=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='bookshop',
            name='address1',
            field=models.CharField(max_length=200, null=True, verbose_name='Address', blank=True),
        ),
        migrations.AlterField(
            model_name='bookshop',
            name='address2',
            field=models.CharField(max_length=200, null=True, verbose_name='address 2', blank=True),
        ),
        migrations.AlterField(
            model_name='bookshop',
            name='city',
            field=models.CharField(max_length=200, null=True, verbose_name='City', blank=True),
        ),
        migrations.AlterField(
            model_name='bookshop',
            name='comment',
            field=models.TextField(null=True, verbose_name='Comment', blank=True),
        ),
        migrations.AlterField(
            model_name='bookshop',
            name='country',
            field=models.CharField(max_length=200, null=True, verbose_name='Country', blank=True),
        ),
        migrations.AlterField(
            model_name='bookshop',
            name='email',
            field=models.EmailField(max_length=254, null=True, verbose_name='email', blank=True),
        ),
        migrations.AlterField(
            model_name='bookshop',
            name='firstname',
            field=models.CharField(max_length=200, null=True, verbose_name='First name', blank=True),
        ),
        migrations.AlterField(
            model_name='bookshop',
            name='mobilephone',
            field=models.CharField(max_length=200, null=True, verbose_name='mobile phone', blank=True),
        ),
        migrations.AlterField(
            model_name='bookshop',
            name='name',
            field=models.CharField(max_length=200, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='bookshop',
            name='state',
            field=models.CharField(max_length=200, null=True, verbose_name='State', blank=True),
        ),
        migrations.AlterField(
            model_name='bookshop',
            name='telephone',
            field=models.CharField(max_length=200, null=True, verbose_name='telephone', blank=True),
        ),
        migrations.AlterField(
            model_name='bookshop',
            name='website',
            field=models.CharField(max_length=200, null=True, verbose_name='website', blank=True),
        ),
        migrations.AlterField(
            model_name='bookshop',
            name='zip_code',
            field=models.CharField(max_length=200, null=True, verbose_name='zip code', blank=True),
        ),
        migrations.AlterField(
            model_name='card',
            name='currency',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='currency', choices=[(b'euro', b'\xe2\x82\xac'), (b'chf', b'CHF'), (b'CFA', b'CFA'), (b'USD', b'$'), (b'GBP', b'\xc2\xa3'), (b'peso', b'$U')]),
        ),
        migrations.AlterField(
            model_name='card',
            name='img2_crop',
            field=models.CharField(blank=True, max_length=100, null=True, choices=[('center', 'centered'), ('wide', 'fit to width'), ('tall', 'fit to height')]),
        ),
        migrations.AlterField(
            model_name='card',
            name='img3_crop',
            field=models.CharField(blank=True, max_length=100, null=True, choices=[('center', 'centered'), ('wide', 'fit to width'), ('tall', 'fit to height')]),
        ),
        migrations.AlterField(
            model_name='card',
            name='img4_crop',
            field=models.CharField(blank=True, max_length=100, null=True, choices=[('center', 'centered'), ('wide', 'fit to width'), ('tall', 'fit to height')]),
        ),
        migrations.AlterField(
            model_name='card',
            name='title_ascii',
            field=models.CharField(max_length=200, null=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='client',
            name='address1',
            field=models.CharField(max_length=200, null=True, verbose_name='Address', blank=True),
        ),
        migrations.AlterField(
            model_name='client',
            name='address2',
            field=models.CharField(max_length=200, null=True, verbose_name='address 2', blank=True),
        ),
        migrations.AlterField(
            model_name='client',
            name='city',
            field=models.CharField(max_length=200, null=True, verbose_name='City', blank=True),
        ),
        migrations.AlterField(
            model_name='client',
            name='comment',
            field=models.TextField(null=True, verbose_name='Comment', blank=True),
        ),
        migrations.AlterField(
            model_name='client',
            name='country',
            field=models.CharField(max_length=200, null=True, verbose_name='Country', blank=True),
        ),
        migrations.AlterField(
            model_name='client',
            name='email',
            field=models.EmailField(max_length=254, null=True, verbose_name='email', blank=True),
        ),
        migrations.AlterField(
            model_name='client',
            name='firstname',
            field=models.CharField(max_length=200, null=True, verbose_name='First name', blank=True),
        ),
        migrations.AlterField(
            model_name='client',
            name='mobilephone',
            field=models.CharField(max_length=200, null=True, verbose_name='mobile phone', blank=True),
        ),
        migrations.AlterField(
            model_name='client',
            name='name',
            field=models.CharField(max_length=200, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='client',
            name='state',
            field=models.CharField(max_length=200, null=True, verbose_name='State', blank=True),
        ),
        migrations.AlterField(
            model_name='client',
            name='telephone',
            field=models.CharField(max_length=200, null=True, verbose_name='telephone', blank=True),
        ),
        migrations.AlterField(
            model_name='client',
            name='website',
            field=models.CharField(max_length=200, null=True, verbose_name='website', blank=True),
        ),
        migrations.AlterField(
            model_name='client',
            name='zip_code',
            field=models.CharField(max_length=200, null=True, verbose_name='zip code', blank=True),
        ),
        migrations.AddField(
            model_name='institutionbasket',
            name='client',
            field=models.ForeignKey(blank=True, to='search.Client', null=True),
        ),
    ]
