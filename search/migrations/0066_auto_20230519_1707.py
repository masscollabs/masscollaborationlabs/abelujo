# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0065_auto_20230223_1218'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='card',
            name='price_bought',
        ),
        migrations.RemoveField(
            model_name='distributor',
            name='stars',
        ),
    ]
