# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0052_auto_20220118_1655'),
    ]

    operations = [
        migrations.AddField(
            model_name='distributor',
            name='address1',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='distributor',
            name='address2',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
    ]
