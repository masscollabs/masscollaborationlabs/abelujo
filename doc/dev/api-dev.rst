Working with the API
====================

Set your ``API_TOKEN`` in the ``settings.py``.

Some endpoints:

- ``/api/cards/`` arguments: ``with_quantity=true``, ``with_dilicom=true``

  Results are cached for 20 hours, including when with_dilicom is set to true.

- ``/api/datasource/search/?query=9782372090674`` where query can also be free text.

  Depending on the query type, search will be done on a different data
  source: if it's an ISBN and a pro vendor is configured, ask it. If
  it is free text and a pro vendor that supports free search is
  configured (Electre, not Dilicom), use it. Otherwise, use the
  default data source (web scraper).

  Results are cached.



Stripe payments API
-------------------

Here's an example payload the website client must send to Abelujo to initiate a reservation/command.

``abelujo_items`` contains references to books already in the database. ``search_items`` come from a wider catalogue search (Electre), books are not in the DB and must be fetched before.::

  {
    "buyer": {
      "billing_address": {
        "last_name": "Test",
        "first_name": "Lucas",
        "email": "lucas@test.design",
        "address": "6 impasse du code",
        "address_comp": "sd",
        "city": "Code Land",
        "postcode": "30000",
        "country": "France",
        "phone": "0600000000"
      },
      "delivery_address": {
        "last_name": "Test",
        "first_name": "Lucas",
        "email": "lucas@test.design",
        "address": "6 impasse du code",
        "address_comp": "",
        "city": "Code Land",
        "postcode": "30000",
        "country": "France",
        "phone": "0600000000"
      }
    },
    "order": {
      "online_payment": false,
      "shipping_method": "Retrait local",
      "mondial_relay_AP": "",
      "amount": 15.3,
      "abelujo_items": [
        {
          "id": 1974,
          "quantity": 1
        }
      ],
      "search_items": [
        {
          "isbn": "9782742720682",
          "price": 9.2,
          "publisher": "Actes sud",
          "quantity": 1
        }
      ],
      "stripe_payload": {
        "payment_method_types": [
          "card"
        ],
        "line_items": [
          {
            "price_data": {
              "currency": "eur",
              "product_data": {
                "name": "Antigone"
              },
              "unit_amount": 610
            },
            "quantity": 1
          },
          {
            "price_data": {
              "currency": "eur",
              "product_data": {
                "name": "Antigone"
              },
              "unit_amount": 919.9999999999999
            },
            "quantity": 1
          }
        ],
        "mode": "payment",
        "customer_email": "lucas@test.design",
        "success_url": "http://localhost:8081/url-modale-ok",
        "cancel_url": "http://localhost:8081/url-modale-erreur"
      },
      "used_promo_code": ""
    }
  }

 See the file ``payload-stripe-api.json`` in this documentation directory and the unit tests.

 Endpoints
 ---------

 First, handle the first request by the client website::

       url(r'^api/checkout/?', 'search.api_stripe.api_stripe', name='api_stripe_checkout'),

The JSON payload is in the request body.

 Then, handle the validation sent by Stripe::

       url(r'^api/webhooks/?', 'search.api_stripe.api_stripe_hooks', name='api_stripe_hooks'),

Devel
-----

If you save the payload into a .json file, you can send it in a POST request body with ``httpie``::

  http POST :8000/api/checkout < payload-stripe-api.json
