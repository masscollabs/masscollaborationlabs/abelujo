Working with Dilicom
====================

Abelujo supports the "FEL à la demande par web service" by Dilicom.

*It is a french books data provider, so let's explain everything in french ;)*

Même si vous avez un compte client Dilicom qui vous permet d'utiliser
leur interface web, vous devez souscrire à ce service pour que votre
logiciel puisse utiliser le FEL à la demande par web service.

Vous devez souscrire à leur service:

- FEL à la demande par web service
- format des commandes: format text plat.

Abelujo peut envoyer la liste de commandes directement à Dilicom, mais
cela a l'inconvénient que vous ne trouverez plus vos commandes dans
les paniers Dilicom, elles sont directement envoyées chez les
distributeurs. Ce service n'est pas activé par défaut, n'hésitez pas à
prendre contact avec nous.

À ce jour Abelujo ne prend pas en charge le format du FEL complet au
format Onix. Les besoins de nos clients sont couverts par le FEL à la
demande, et les coûts du FEL complet sont bien supérieurs. N'hésitez
pas à nous demander un devis.

De plus, Abelujo prend en charge l'API d'Electre, qui vous permet de
bénéficier de la qualité de leurs notices et vous permet de lancer des
recherches croisées dans toute la base du livre français directement
depuis Abelujo.


Données professionnelles
----------------------

Dilicom fournit bien plus de données que ce que propose Abelujo par défaut avec les données internet. Vous trouverez en plus:

- le plus important, la disponibilité
- et le distributeur
- le thème de la CLIL
- la collection
- la présentation éditeur (broché, présentoir etc)
- les dimensions et le poids
- les détails du prix et de sa ou ses TVAs
- et plus…


Données manquantes
------------

Ces données ne sont pas fournies par le FEL à la demande par web service:

- images
- résumé
- nombre de pages

Pour les images, vous pouvez vous abonner à leur service de distribution d'images.

Elles sont toutes disponibles dans le FEL complet ou dans Electre.


Lancer Abelujo avec les identifiants Dilicom
--------------------------------------------

  DILICOM_USER=GLN_librairie DILICOM_PASSWORD=bar make gunicorn


Working with the API
--------------------

``/api/card/:id?with_dilicom_update=1`` returns the book data, and calls
Dilicom to update data (the price, the publisher, the
distributor…). Dilicom messages are returned in
`alerts.dilicom_update`. If the ISBN was not found on Dilicom, the
card object gets `dilicom_unknown_ean` set to True (it is normally
false).

Results are cached for 20 hours.

Settings
--------

See settings on Installation, for example FEATURE_UPDATE_CARD_ON_SELL.
