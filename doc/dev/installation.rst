Installation - locally and on a server
======================================


See the up to date instructions `on Gitlab <https://gitlab.com/vindarel/abelujo>`_.

See the Makefile for all the targets.

Run the development server (local)
----------------------------------

`make run` and open your browser on `localhost:8000`.

You must create a user (see management commands to create and delete
superusers in the next section).

This command is only to be run on a local machine. If you run it on a
server, you won't be able to access Abelujo from the internet. But you could do::

  ./manage.py runserver PORT:IP


Run in production (server)
--------------------------

Put your ip address in the file `IP.txt` (root of the project) and the
port in `PORT.txt`::

    echo "my.ip" > IP.txt
    echo "8888" > PORT.txt

and run the server with ``make gunicorn``.

To restart the application, use ``make gunicorn-restart``.

Some tasks need to run asynchronously (applying an inventory,…). Start
 the asyncronous task runner with::

    make taskqueue

Note: this method to make an inventory is deprecated.

If you are subscribed to Dilicom, see `Dilicom documentation <dilicom.html >`_.


Deployment and website management
---------------------------------

We use gunicorn and whitenoise to serve static files.

- http://docs.gunicorn.org/en/latest/
- https://github.com/evansd/whitenoise


Setup Sentry (issues monitoring)
--------------------------------

Sentry helps immensely in being notified of runtime errors.

The settings read the
Sentry token in a `sentry.txt` file if present.

Test with `python manage.py raven test`.


Debian specificity: name collision between node and nodejs
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This is installed with `make debian`::

    sudo apt-get install nodejs-legacy

Use the Postgresql database
---------------------------

(but you can stay with SQLite, it works with millions of records)

Abelujo uses by default an SQLite database, you don't *have* to
configure a Postgresql one.

We need to create a database with the following steps::

    sudo su - postgres
    createdb abelujo_db
    createuser abelujo_user -P # and enter a password.

Now call a postgresql prompt::

    psql
    GRANT ALL PRIVILEGES ON DATABASE abelujo_db TO abelujo_user;

Now we have a postgre database and a user ("postgres") to use it. We
just have to put it in our ``abelujo/settings.py``.

Create the db::

    ./manage.py syncdb

Note: if we have "authentication peer failed for user XXX", edit the
file ``/etc/postgresql/9.3/main/pg_hba.conf`` (which lists
permissions) and change::

    local all all peer

to::

    local all all trust


Populate the DB with our initial data
--------------------------------------

We may enjoy some initial data to start working with Abelujo: typical
book categories, default places and basket(s), etc. They will be
different depending on the user's needs and language.

This command is required::

  make db

In the next topic, see the available management commands.


Post-installation: set personal settings
----------------------------------------

Set the default datasource: if you want to search on the Swiss source
by default, use a shell variable::

  export DEFAULT_DATASOURCE='lelivre'

and then start Abelujo.

Set other variables in a configuration file.

Create a file ``config.py`` at the project root.

Make it start with the following line::

  # -*- coding: utf-8 -*-

Abelujo will read the following variables at startup:

* PAYMENT_CHOICES, for the sell page. For example::

  PAYMENT_CHOICES = [
    (1, "ESPÈCES"),
    (7, "MAESTRO"),
    (10, "MASTERCARD"),
    (11, "VISA"),
    (12, "autre"),
    (100, "bon d'achat"),
  ]

(see ``models/common.py`` for the default ones)

Each entry must have a distinct id.

An id greater than 100 will not be counted in the total revenue of
that day. It is typicall for coupons: you sold a coupon (bon d'achat)
to a client last month, they use it today: you don't want to count
this sell twice.

In the database, each "CardType" represents a type of product: book,
CD, newspaper, etc. You can associate a VAT to a product type directly
in the admin panel.

That being said, if you have a lot of similar types, you can also choose a default VAT value::


  VAT_TAXES = {
      'default': 20,
  }

Be sure to choose the VAT for the "book" type, and then you can use this setting.


* ``FEATURE_UPDATE_CARD_ON_SELL``: set to True to fire providers' requests to check for a price update during a sell. It was on, it is now disabled (v0.20), because we can easily exceed the providers' plan (Dilicom: 40,000 requests per year, not much).



API Authentication tokens
-------------------------

The API token is used to protect the API endpoints.

In ``config.py`` set::

    FEATURE_USE_API_TOKEN = True  # False by default for backward compatibility of web clients purposes.
    API_TOKEN = "secret"



Post-installation: configure a Twilio account to send SMS
---------------------------------------------------------

Since January, 2021, Abelujo supports sending text messages (SMS) to
clients, for example from the Reservations page. You need to create a
Twilio account (https://www.twilio.com/) and configure 2 or 3
variables in ``config.py``::

    FEATURE_SMS = True
    # twilio-python also searches for these tokens as env variables.
    TWILIO_ACCOUNT = ""
    TWILIO_TOKEN = ""
    TWILIO_SENDER = ""  # the sender phone number, to find in your Twilio account.
    TWILIO_SENDER_NAME = "Librairie"  # (optional). A custom sender name. Must alphanum, no spaces, at most 11 characters.

The Abelujo app should restart and you will see colored messages
indicating that these settings were taken into account. You will also
find new buttons in the Reservations page.
