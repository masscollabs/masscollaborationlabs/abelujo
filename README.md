Abelujo - free software to manage independent book (and records) shops.
=======================================================================

français: [Lisez-moi](https://gitlab.com/vindarel/abelujo/blob/master/README_fr.md "README en français")

This project is used daily by individuals, associations, tea
shops, theaters, art centers and professional bookshops.

Abelujo allows to:

-   look up for **books**, either by keywords or by isbn/ean (which works with a **barcode scanner**). See the https://gitlab.com/vindarel/bookshops library. You can currently search for:

    * french books:
      * through [Dilicom](https://dilicom-prod.centprod.com) (the profesional provider)
      * through [Electre](https://electre.com/) (the profesional provider)
      * and a couple external sources that don't need registration, with less data and for non-commercial use.
    * spanish books (through [casadellibro.com](http://www.casadellibro.com)) ![](http://gitlab.com/vindarel/bookshops/badges/master/build.svg?job=spanish_scraper)
    * german books(through [buchlentner.de](http://www.buchlentner.de)) ![](http://gitlab.com/vindarel/bookshops/badges/master/build.svg?job=german_scraper)
-   look up for **CDs** (via [discogs.com](http://www.discogs.com/))
-   register books efficiently,
-   sell books, see the history, etc.

It is translated to English, French and Spanish.

**Abelujo** means Beehive in Esperanto.

Enjoy!

<a href="https://liberapay.com/vindarel/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>

![looking for a registered card](doc/abelujo-search-isbn.png)

Installation
============

**note: we have a Python 3 and Django 2 port.** If you need to install
Abelujo on a recent distribution, look at the `django20py3` branch. It
uses Python 3 (tested on Python 3.8) and one of the latest Django
version (2.2). For stability, stay with master.

**Python 3 status**: it works and it is rebased on master, but it is
not fully tested. Use at your own risk.

**If you want to try out a rewrite that is easy to install (a single executable), drop us an email**. However, it doesn't have all Abelujo features so far.

Instructions for Debian 9 and 10.

[![build status](https://gitlab.com/vindarel/abelujo/badges/master/build.svg)](https://gitlab.com/vindarel/abelujo/commits/master)

Either do the quick way:

    apt install curl -y && curl -sS https://gitlab.com/vindarel/abelujo/raw/master/install.sh | bash -

this will clone the repo in the current directory and install all its dependencies.

or read below for the detailed instructions.

See after to install without sudo.

Get the sources:

    git clone --recursive https://gitlab.com/vindarel/abelujo.git

it creates the directory "abelujo":

    cd abelujo

Ensure you have Python 2.7.

Install the required dependencies for Debian (Ubuntu/LinuxMint/etc):

    make debian
    # a shortcut for
    # sudo apt-get install python-pip
	# sudo pip install virtualenvwrapper

Create and activate a virtual environment (so than we can install python
libraries locally, not globally to your system). Do as you are used to,
or do the following:

    source venv_create.sh # [venvname] (optional argument)
    workon abelujo

now your shell prompt should show you are in the `abelujo`
virtualenv. To quit the virutal env, type `deactivate`. To enter it,
type `workon \<TAB\> abelujo`.


To install the dependencies, create and populate the database, run:

    make install

We are done ! Now to try Abelujo, run the development server like this:

    make run
    # or set the port with:
    # python manage.py runserver 9876

and open your browser to <http://127.0.0.1:8000> (admin/admin).

You might need to create a superuser: see [the dev documentation](http://dev.abelujo.cc/use-manage.html).

Update
------

Use any of:

    make update

to update everything: system dependencies, Python dependencies, etc. The longest, the safiest. Required when you didn't update in a long time.

    make update-code

to update only the code and perform the minimal necessary update tasks. Does not try to install dependencies. Faster than the above. Often enough for quick updates.

    make update-latesttag

to update to the latest repository tag, not master. Often safer, should avoid bugs happening on the latest master commits.


Enable Systemd
--------------

Create such a systemd file like this:

    sudoedit /etc/systemd/system/abelujo.service

```
Description=Abelujo

[Service]
User=debian
WorkingDirectory=/home/yourself/path/to/abelujo/
# Environment="DILICOM_USER=330gencode"
# Environment="DILICOM_PASSWORD=password"
ExecStart=/home/yourself/.venvs/abelujo/bin/gunicorn --env DJANGO_SETTINGS_MODULE=abelujo.settings abelujo.wsgi --bind=your.real.external.ip:8000 --reload --pid=PID.txt
Type=simple
Restart=on-failure

[Install]
WantedBy=network.target
```

You need to adapt:

- your user ("debian"). By default, a Systemd service is run as root.
- your username ("yourself")
- the path to your virtual. Check that "/home/yourself/.venvs/" is a directory, check that "/home/yourself/.venvs/bin/gunicorn" is a program.
- your ip address, the port.
- check that you can copy-paste the ExecStart line and it starts Abelujo.

Start the app once:

    sudo systemctl start abelujo.service

Enable the service at system (re)boot:

    sudo systemctl enable abelujo.service

See logs:

    journalctl -u abelujo.service

Just a note, we don't see our startup logs at startup from the settings, saying if Dilicom or Electre are properly configured.


Install without sudo
--------------------

    make debian-nosudo
    make install-nosudo

please read the Makefile in this particular case.


How to update
-------------

To update, you need to: pull the sources (`git pull --rebase`),
install new packages (system and python-wide), run the database
migrations, build the static assets and, in production, collect the
static files.

In the virtual env, run:

    make update
    # git pull --rebase
    # git submodule update --remote
    # install pip, migrate, compile transalation files.

Actions required for updates
----------------------------

- for version **0.8** (january, 24th 2020): update pip, run `make
update`, and start the new task queue with `make taskqueue`. You don't
need Redis anymore.

Development
===========

Django project, in python (2.7) (3.8 up to date in its own branch, less tested) and JavaScript (AngularJS).

We use:

- [LiveScript](http://livescript.net)
- [jade templates](http://jade-lang.com/) (now Pug), which compile to html,
    and pyjade for the Django integration
- [Bootstrap's CSS](http://getbootstrap.com) with django-bootstrap3

See the developer documentation: http://dev.abelujo.cc/.

This is [our database graph](http://dev.abelujo.cc/graph-db.png)
(`make graphdb`).

### Dev installation ###

As a complement to the installion procedure above, you also need to
install development dependencies that are listed in another
requirements file::

    make pip-dev

and npm packages

    make npm
    # this installs node dependencies

Then, to build the JS and CSS:

    make gulp

and commit the result.

Also install npm packages to run end to end tests:

    make npm-dev  # packages listed in devDependencies of packages.json


### Vagrant and Ansible role

You can install and try everything in a Vagrant virtual machine, thanks to a nice contribution: https://gitlab.com/anarchistcat/abelujo-ansible

## Configure services

### Task queue (mandatory)

We need a task queue for long operations (notably applying an inventory to the stock). We use Django-q in pure-Django mode (no more Redis).

Start it with

    make task-queue &
    # aka honcho start &

### Sentry (optional)

Put your
[Sentry](https://docs.sentry.io/clients/python/integrations/django/)
private token in a `sentry.txt` file. The settings will see and read
it.

To get Fabric send it to the remote instance on install (`fab install`
calls `fab save_variables`), add the token into your `clients.yaml`
under `sentry_token` (see the fabfile).

Test with `python manage.py raven test` and see the new message in your dashboard.


### Electre API (optional)

We support fetching data on Electre's API. We can search by ISBN or with a free text search.

The `pyelectre` module is developed separately, you will to get in touch with us.


Run unit tests
--------------

    make unit

Code coverage:

    make cov  # and open your browser at htmlcov/index.html

Testing the installation script in Docker (experimental)
--------------------------------------------------------

Given you have Docker already installed, run the installation script
in a fresh Ubuntu 16.04 with:

    chmod +x dockentry.sh
    docker run  -v "$(pwd)/docker":/home/docker/ -ti ubuntu:16.04 /home/docker/dockentry.sh

The script given as argument creates a user with sudo rights and then
calls the installation script.

You can also simply step into the image and run scripts manually from
there.

See a bit more in doc/dev/ci.rst.


Load data
---------

See the scripts in `scripts/` to load data (specially shelves
names), in different languages.

Troubleshooting
---------------

If you get:

    OperationalError: no such column: search_card.card_type_id

it is probably because you pulled the sources and didn't update your
DB. Use database migrations (`make migrate`).

Uninstall
---------

To uninstall Javascript and Python libraries, see `make uninstall[-js, -pip]`.

The most worth it is to uninstall JS libs from `node_modules`, that
frees a couple MB up.


Documentation
-------------

We have developer documentation: http://dev.abelujo.cc/

Licence
-------

AGPL-3.0
